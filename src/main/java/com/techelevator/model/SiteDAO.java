package com.techelevator.model;

import java.util.Date;
import java.util.List;

public interface SiteDAO {
	
//	public List<Site> getAvailableSites(int campgroun_id, Date startDate, Date endDate);
//	public List<Site> parkGetAvailableSites(int park_id, Date startDate, Date endDate);
	public int addSiteReservation(int siteId, String name, Date fromDate, Date toDate);
	double getCostFromSite(int site_id);
	public List<Site> getAvailableSites(int campground_id, Date startDate, Date endDate, int max_occupancy,
			boolean accessible, int max_rv_length, boolean utilities);
	public List<Site> parkGetAvailableSites(int park_id, Date startDate, Date endDate, int max_occupancy,
			boolean accessible, int max_rv_length, boolean utilities);
	
}
