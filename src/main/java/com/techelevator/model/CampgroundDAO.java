package com.techelevator.model;

import java.util.List;

public interface CampgroundDAO {
	
	public List<Campground> getAllCampgrounds(int park_id);
	public Campground getCampFromId(int campground_id);

}
