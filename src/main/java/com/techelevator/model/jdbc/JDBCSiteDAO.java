package com.techelevator.model.jdbc;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.model.Site;
import com.techelevator.model.SiteDAO;

public class JDBCSiteDAO implements SiteDAO {
	
	private JdbcTemplate jdbcTemplate;
	
	public JDBCSiteDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

//	@Override
//	public List<Site> getAvailableSites(int campground_id, Date startDate, Date endDate) {
//		ArrayList<Site> sites = new ArrayList<Site>();
//		
//		String sqlFindSites = "SELECT * FROM site " +
//							  "WHERE site.campground_id = ? " +
//							  "AND site.campground_id IN ( " +
//							  	 "SELECT campground_id " +
//							  	 "FROM campground " +
//							  	 "WHERE EXTRACT(MONTH FROM CAST(? AS DATE)) BETWEEN CAST(open_from_mm AS INTEGER) AND CAST(open_to_mm AS INTEGER)" +
//							  ") AND site.campground_id IN ( " +
//							  	 "SELECT campground_id " +
//							  	 "FROM campground " +
//							  	 "WHERE EXTRACT(MONTH FROM CAST(? AS DATE)) BETWEEN CAST(open_from_mm AS INTEGER) AND CAST(open_to_mm AS INTEGER)" +
// 							  ") AND site.site_id NOT IN ( " +
//							  	 "SELECT site_id " +
//								 "FROM reservation " +
//								 "WHERE (from_date >= ? AND from_date < ?) " +
//								 "OR (to_date > ? AND to_date <= ?)" +
//							  ") ORDER BY site.site_number " +
//							  "LIMIT 5";
//
//		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlFindSites, campground_id, startDate, endDate, startDate, endDate, startDate, endDate);
//		while(results.next()) {
//			Site site = mapRowToSite(results);
//			sites.add(site);
//		}
//		return sites;
//	}
	
	@Override
	public List<Site> getAvailableSites(int campground_id, Date startDate, Date endDate, int max_occupancy, boolean accessible, int max_rv_length, boolean utilities) {
		ArrayList<Site> sites = new ArrayList<Site>();
		String sqlFindSites = "";
		
		if (utilities) {
			sqlFindSites = "SELECT * FROM site " +
							  "WHERE site.campground_id = ? " +
							  "AND site.campground_id IN ( " +
							  	 "SELECT campground_id " +
							  	 "FROM campground " +
							  	 "WHERE EXTRACT(MONTH FROM CAST(? AS DATE)) BETWEEN CAST(open_from_mm AS INTEGER) AND CAST(open_to_mm AS INTEGER)" +
							  ") AND site.campground_id IN ( " +
							  	 "SELECT campground_id " +
							  	 "FROM campground " +
							  	 "WHERE EXTRACT(MONTH FROM CAST(? AS DATE)) BETWEEN CAST(open_from_mm AS INTEGER) AND CAST(open_to_mm AS INTEGER)" +
 							  ") AND site.site_id NOT IN ( " +
							  	 "SELECT site_id " +
								 "FROM reservation " +
								 "WHERE (from_date >= ? AND from_date < ?) " +
								 "OR (to_date > ? AND to_date <= ?)" +
							  ") AND site.max_occupancy >= ? " +
							  "AND site.accessible = ? " +
							  "AND site.max_rv_length >= ? " +
							  "AND site.utilities = TRUE " +
							  "ORDER BY site.site_number " +
							  "LIMIT 5";
		} else {
			sqlFindSites = "SELECT * FROM site " +
					  "WHERE site.campground_id = ? " +
					  "AND site.campground_id IN ( " +
					  	 "SELECT campground_id " +
					  	 "FROM campground " +
					  	 "WHERE EXTRACT(MONTH FROM CAST(? AS DATE)) BETWEEN CAST(open_from_mm AS INTEGER) AND CAST(open_to_mm AS INTEGER)" +
					  ") AND site.campground_id IN ( " +
					  	 "SELECT campground_id " +
					  	 "FROM campground " +
					  	 "WHERE EXTRACT(MONTH FROM CAST(? AS DATE)) BETWEEN CAST(open_from_mm AS INTEGER) AND CAST(open_to_mm AS INTEGER)" +
					  ") AND site.site_id NOT IN ( " +
					  	 "SELECT site_id " +
						 "FROM reservation " +
						 "WHERE (from_date >= ? AND from_date < ?) " +
						 "OR (to_date > ? AND to_date <= ?)" +
					  ") AND site.max_occupancy >= ? " +
					  "AND site.accessible = ? " +
					  "AND site.max_rv_length >= ? " +
					  "ORDER BY site.site_number " +
					  "LIMIT 5";
		}
		
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlFindSites, campground_id, startDate, endDate, startDate, endDate, startDate, endDate, max_occupancy, accessible, max_rv_length);
		while(results.next()) {
			Site site = mapRowToSite(results);
			sites.add(site);
		}
		return sites;
	}
	
//	@Override
//	public List<Site> parkGetAvailableSites(int park_id, Date startDate, Date endDate) {
//		ArrayList<Site> sites = new ArrayList<Site>();
//		
//		String sqlFindSites = "SELECT * FROM site " +
//							  "INNER JOIN campground c ON site.campground_id = c.campground_id " +
//							  "INNER JOIN park p ON c.park_id = p.park_id " +
//							  "WHERE p.park_id = ? " +
//							  "AND site.campground_id IN ( " +
//							  	 "SELECT campground_id " +
//							  	 "FROM campground " +
//							  	 "WHERE EXTRACT(MONTH FROM CAST(? AS DATE)) BETWEEN CAST(open_from_mm AS INTEGER) AND CAST(open_to_mm AS INTEGER)" +
//							  ") AND site.campground_id IN ( " +
//							  	 "SELECT campground_id " +
//							  	 "FROM campground " +
//							  	 "WHERE EXTRACT(MONTH FROM CAST(? AS DATE)) BETWEEN CAST(open_from_mm AS INTEGER) AND CAST(open_to_mm AS INTEGER)" +
// 							  ") AND site.site_id NOT IN ( " +
//							  	 "SELECT site_id " +
//								 "FROM reservation " +
//								 "WHERE (from_date >= ? AND from_date < ?) " +
//								 "OR (to_date > ? AND to_date <= ?)" +
//							  ") ORDER BY site.site_number " +
//							  "LIMIT 5";
//		
//		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlFindSites, park_id, startDate, endDate, startDate, endDate, startDate, endDate);
//		while(results.next()) {
//			Site site = mapRowToSite(results);
//			sites.add(site);
//		}
//		return sites;
//	}
	
	@Override
	public List<Site> parkGetAvailableSites(int park_id, Date startDate, Date endDate, int max_occupancy, boolean accessible, int max_rv_length, boolean utilities) {
		ArrayList<Site> sites = new ArrayList<Site>();
		String sqlFindSites = "";
		
		if (utilities) {
			sqlFindSites = "SELECT * FROM site " +
								  "INNER JOIN campground c ON site.campground_id = c.campground_id " +
								  "INNER JOIN park p ON c.park_id = p.park_id " +
								  "WHERE p.park_id = ? " +
								  "AND site.campground_id IN ( " +
								  	 "SELECT campground_id " +
								  	 "FROM campground " +
								  	 "WHERE EXTRACT(MONTH FROM CAST(? AS DATE)) BETWEEN CAST(open_from_mm AS INTEGER) AND CAST(open_to_mm AS INTEGER)" +
								  ") AND site.campground_id IN ( " +
								  	 "SELECT campground_id " +
								  	 "FROM campground " +
								  	 "WHERE EXTRACT(MONTH FROM CAST(? AS DATE)) BETWEEN CAST(open_from_mm AS INTEGER) AND CAST(open_to_mm AS INTEGER)" +
	 							  ") AND site.site_id NOT IN ( " +
								  	 "SELECT site_id " +
									 "FROM reservation " +
									 "WHERE (from_date >= ? AND from_date < ?) " +
									 "OR (to_date > ? AND to_date <= ?)" +
								  ") AND site.max_occupancy >= ? " +
								  "AND site.accessible = ? " +
								  "AND site.max_rv_length >= ? " +
								  "AND site.utilities = TRUE " +
								  "ORDER BY site.site_number " +
								  "LIMIT 5";
		} else {
			sqlFindSites = "SELECT * FROM site " +
						   "INNER JOIN campground c ON site.campground_id = c.campground_id " +
						   "INNER JOIN park p ON c.park_id = p.park_id " +
						   "WHERE p.park_id = ? " +
						   "AND site.campground_id IN ( " +
						  	  "SELECT campground_id " +
						  	  "FROM campground " +
						  	  "WHERE EXTRACT(MONTH FROM CAST(? AS DATE)) BETWEEN CAST(open_from_mm AS INTEGER) AND CAST(open_to_mm AS INTEGER)" +
						   ") AND site.campground_id IN ( " +
						  	  "SELECT campground_id " +
						  	  "FROM campground " +
						  	  "WHERE EXTRACT(MONTH FROM CAST(? AS DATE)) BETWEEN CAST(open_from_mm AS INTEGER) AND CAST(open_to_mm AS INTEGER)" +
						   ") AND site.site_id NOT IN ( " +
						  	  "SELECT site_id " +
							  "FROM reservation " +
							  "WHERE (from_date >= ? AND from_date < ?) " +
							  "OR (to_date > ? AND to_date <= ?)" +
						   ") AND site.max_occupancy >= ? " +
						   "AND site.accessible = ? " +
						   "AND site.max_rv_length >= ? " +
						   "ORDER BY site.site_number " +
						   "LIMIT 5";
		}
		
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlFindSites, park_id, startDate, endDate, startDate, endDate, startDate, endDate, max_occupancy, accessible, max_rv_length);
		while(results.next()) {
			Site site = mapRowToSite(results);
			sites.add(site);
		}
		return sites;
	}
	
	@Override
	public double getCostFromSite(int site_id) {
		double cost = 0;
		
		String sqlGetCost = "SELECT daily_fee FROM campground " +
							"INNER JOIN site ON site.campground_id = campground.campground_id " +
							"WHERE site_id = ?";
		
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetCost, site_id);
		if (results.next()) {
			cost = results.getDouble("daily_fee");
			return cost;
		} else {
			return 0;
		}
	}
	
	public int addSiteReservation(int siteId, String name, Date fromDate, Date toDate) {
		String sqlAddReservation = "INSERT INTO reservation (site_id, name, from_date, to_date) VALUES (?, ?, ?, ?)";
		String sqlGetResId = "SELECT * FROM reservation WHERE name = ? AND from_Date = ? AND site_id = ?";
		jdbcTemplate.update(sqlAddReservation, siteId, name, fromDate, toDate);
		
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlGetResId, name, fromDate, siteId);
		if (results.next()) {
			int resId = 0;
			resId = results.getInt("reservation_id");
			return resId;
		} else {
			return 0;
		}
	}
	
	private Site mapRowToSite(SqlRowSet results) {
		Site site = new Site();
		site.setAccessible(results.getBoolean("accessible"));
		site.setCampground_id(results.getInt("campground_id"));
		site.setMax_occupancy(results.getInt("max_occupancy"));
		site.setMax_rv_length(results.getInt("max_rv_length"));
		site.setSite_id(results.getInt("site_id"));
		site.setSite_number(results.getInt("site_number"));
		site.setUtilities(results.getBoolean("utilities"));
		
		return site;
	}
}
