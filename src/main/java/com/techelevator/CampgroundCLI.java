package com.techelevator;

import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Formatter;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;

import com.techelevator.model.Campground;
import com.techelevator.model.CampgroundDAO;
import com.techelevator.model.Park;
import com.techelevator.model.ParkDAO;
import com.techelevator.model.Reservation;
import com.techelevator.model.ReservationDAO;
import com.techelevator.model.Site;
import com.techelevator.model.SiteDAO;
import com.techelevator.model.jdbc.JDBCCampgroundDAO;
import com.techelevator.model.jdbc.JDBCParkDAO;
import com.techelevator.model.jdbc.JDBCReservationDAO;
import com.techelevator.model.jdbc.JDBCSiteDAO;
import com.techelevator.view.Menu;

public class CampgroundCLI {
	
	private static final String PARK_MENU_OPTION_VIEW_CAMPGROUNDS = "View Campgrounds";
	private static final String PARK_MENU_OPTION_SEARCH = "Search for Available Sites";
	private static final String PARK_MENU_OPTION_SEARCH_ADVANCED = "Search for Available Sites (Advanced)";
	private static final String PARK_MENU_OPTION_RESERVATIONS = "Search for Upcomming Reservations";
	private static final String PARK_MENU_OPTION_RETURN = "Return to Previous Screen";
	private static final String[] PARK_MENU_OPTIONS = new String[] { PARK_MENU_OPTION_VIEW_CAMPGROUNDS, 
																	 PARK_MENU_OPTION_SEARCH,
																	 PARK_MENU_OPTION_SEARCH_ADVANCED,
																	 PARK_MENU_OPTION_RESERVATIONS,
																	 PARK_MENU_OPTION_RETURN,};
	
	private static final String CAMP_MENU_OPTION_SEARCH_SITES = "Search for Available Sites";
	private static final String CAMP_MENU_OPTION_ADVANCED_SEARCH = "Search for Available Sites (Advanced)";
	private static final String CAMP_MENU_OPTION_RETURN = "Return to Previous Screen";
	private static final String[] CAMP_MENU_OPTIONS = new String[] { CAMP_MENU_OPTION_SEARCH_SITES,
																	 CAMP_MENU_OPTION_ADVANCED_SEARCH,
																	 CAMP_MENU_OPTION_RETURN,};
	
	private Scanner in;
	private Menu menu;
	private ParkDAO parkDAO;
	private CampgroundDAO campgroundDAO;
	private SiteDAO siteDAO;
	private ReservationDAO reservationDAO;

	public static void main(String[] args) {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setUrl("jdbc:postgresql://localhost:5432/campground");
		dataSource.setUsername("postgres");
		dataSource.setPassword("postgres1");
		
		CampgroundCLI application = new CampgroundCLI(dataSource);
		application.run();
	}

	public CampgroundCLI(DataSource dataSource) {
		this.menu = new Menu(System.in, System.out);
		
		parkDAO = new JDBCParkDAO(dataSource);
		campgroundDAO = new JDBCCampgroundDAO(dataSource);
		siteDAO = new JDBCSiteDAO(dataSource);
		reservationDAO = new JDBCReservationDAO(dataSource);
		
	}
	
	public void run() {
		System.out.println("National Park Campsite Reservation");
		System.out.println();
		
		while(true) {
			List<Park> parks = parkDAO.getAllParks();
			
			printHeading("Select a Park for Further Details");
			
			Park selection = (Park) menu.getChoiceFromOptions(parks.toArray());
			System.out.println();
			System.out.println(selection + " National Park");
			System.out.println("Location: "+ selection.getLocation());
			System.out.println("Established: "+ selection.getEstablish_date());
			System.out.println("Area: "+ selection.getArea() +" sq km");
			System.out.println("Annual Visitors: "+ selection.getVisitors());
			System.out.println();
			System.out.println(selection.getDescription());
			
			parkMenuOptions(selection.getPark_id());
		}
		
	}
	
	private void parkMenuOptions(int park_id) {
		while(true) {
			printHeading("Select a Command");
			String choice = (String)menu.getChoiceFromOptions(PARK_MENU_OPTIONS);
			
			if (choice.equals(PARK_MENU_OPTION_VIEW_CAMPGROUNDS)) {
				campgroundMenuOptions(park_id);
			
			} else if (choice.contentEquals(PARK_MENU_OPTION_SEARCH)) {
				Date arrival = getDate("arrival");
				Date departure = getDate("departure");
				parkSiteReservationMenu(park_id, arrival, departure);
				
			} else if (choice.contentEquals(PARK_MENU_OPTION_SEARCH_ADVANCED)) {
				Date arrival = getDate("arrival");
				Date departure = getDate("departure");
				int occupancy = getOccupancy();
				boolean accessible = getAccessible();
				int rvLength = getRvLength();
				boolean utilities = getUtilities();
				parkSiteReservationMenuAdvanced(park_id, arrival, departure, occupancy, accessible, rvLength, utilities);
			
			} else if (choice.contentEquals(PARK_MENU_OPTION_RESERVATIONS)) {
				List<Reservation> reservations = reservationDAO.getUpcomingReservations(park_id);
				printReservations(reservations);
				
			} else if (choice.contentEquals(PARK_MENU_OPTION_RETURN)) {
				break;
			}
		}
	}
	
	private void campgroundMenuOptions(int park_id) {
		List<Campground> campgrounds = campgroundDAO.getAllCampgrounds(park_id);
		
		printCampgrounds(campgrounds);
		
		while(true) {
			String choice = (String)menu.getChoiceFromOptions(CAMP_MENU_OPTIONS);
			if (choice.equals(CAMP_MENU_OPTION_SEARCH_SITES)) {
				printCampgrounds(campgrounds);
				System.out.println();
				Campground selection = getCamp();
				if (selection == null) {
					break;
				}
				Date arrival = getDate("arrival");
				Date departure = getDate("departure");
				siteReservationMenu(selection.getCampground_id(), arrival, departure);
			
			} else if (choice.equals(CAMP_MENU_OPTION_ADVANCED_SEARCH)) {
				printCampgrounds(campgrounds);
				Campground selection = getCamp();
				if (selection == null) {
					break;
				}
				Date arrival = getDate("arrival");
				Date departure = getDate("departure");
				int occupancy = getOccupancy();
				boolean accessible = getAccessible();
				int rvLength = getRvLength();
				boolean utilities = getUtilities();
				
				siteReservationMenuAdvanced(selection.getCampground_id(), arrival, departure, occupancy, accessible, rvLength, utilities);
			} else if (choice.equals(CAMP_MENU_OPTION_RETURN)) {
				break;
			}
		}
	}

	private void siteReservationMenu(int campgroundId, Date startDate, Date endDate) {
		List<Site> sites = siteDAO.getAvailableSites(campgroundId, startDate, endDate, 1, false, 0, false);
		
	    reserveSite(sites, startDate, endDate);	
	}
	
	private void siteReservationMenuAdvanced(int campgroundId, Date startDate, Date endDate, int max_occupancy, boolean accessible, int max_rv_length, boolean utilities) {
		List<Site> sites = siteDAO.getAvailableSites(campgroundId, startDate, endDate, max_occupancy, accessible, max_rv_length, utilities);
		
	    reserveSite(sites, startDate, endDate);	
	}
	
	private void parkSiteReservationMenu(int park_id, Date startDate, Date endDate) {
		List<Site> sites = siteDAO.parkGetAvailableSites(park_id, startDate, endDate, 1, false, 0, false);
		
	    reserveSite(sites, startDate, endDate);
	}
	
	private void parkSiteReservationMenuAdvanced(int park_id, Date startDate, Date endDate, int max_occupancy, boolean accessible, int max_rv_length, boolean utilities) {
		List<Site> sites = siteDAO.parkGetAvailableSites(park_id, startDate, endDate, max_occupancy, accessible, max_rv_length, utilities);
		
	    reserveSite(sites, startDate, endDate);	
	}
	
	private void reserveSite(List<Site> sites, Date startDate, Date endDate) {
		double startDateTime = startDate.getTime();
		double endDateTime = endDate.getTime(); 
	    double numOfDays = 1 + (endDateTime - startDateTime) / (1000*60*60*24);
	    
		if (sites.size() == 0 || startDateTime > endDateTime) {
			System.out.println();
			System.out.println("No sites matched search criteria.");
			
		} else {
			System.out.println("\nResults Matching Your Search Criteria:");
			System.out.println();
			Formatter fmtSiteHeader = new Formatter();
			fmtSiteHeader.format("%-8s %-13s %-13s %-16s %-13s %-13s", "Site ID", "Max Occup.", "Accessible?", "Max RV Length", "Utility", "Total Cost");
			System.out.println(fmtSiteHeader);
			
			for(int i = 0; i < sites.size(); i++) {
				Site site = sites.get(i);
				Double siteCost = siteDAO.getCostFromSite(site.getSite_id());
				Formatter fmtSiteList = new Formatter();
				fmtSiteList.format("%-8s %-13s %-13s %-16s %-13s %-13s", site.getSite_id(), site.getMax_occupancy(), site.isAccessible(), site.getMax_rv_length(), site.isUtilities(), currencyFormat(siteCost * numOfDays));
				System.out.println(fmtSiteList);
		
			}
			System.out.println();
			in = new Scanner(System.in);
			System.out.println("Select site ID you would like to reserve (enter 0 to cancel)");
			int userSite = in.nextInt();
			String userName;
			if (userSite != 0) {
				System.out.println();
				in = new Scanner(System.in);
				System.out.println("What name should the reservation be made under?");
				userName = in.nextLine();
				int resId = siteDAO.addSiteReservation(userSite, userName, startDate, endDate);
				System.out.println();
				System.out.println("Your reservation has been made. Confirmation ID: "+ resId);
			}
		}
	}
	
	private Campground getCamp() {
		Campground camp = null;
		in = new Scanner(System.in);
		System.out.println("Select campground ID# (enter 0 to cancel)");
		int userInput = in.nextInt();
		
		if (userInput == 0) {
			return null;
		}
		
		camp = campgroundDAO.getCampFromId(userInput);
		return camp;
	}
	
	private Date getDate(String input) {
		while(true) {
			Date userDate = null;
			in = new Scanner(System.in);
			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			System.out.println("What is your " + input + " date? (yyyy-mm-dd) ");
			
			String userInput = in.nextLine();
			
			try {
				userDate = dateFormat.parse(userInput);
				return userDate;
			} catch (ParseException e) {
				System.out.println("\n*** "+userInput+" is not a valid option ***\n");
			}
		}
	}
	
	private int getOccupancy() {
		while(true) {
			int userInput = 0;
			in = new Scanner(System.in);
			System.out.println("How many people will be at the site? (Max 12)");
			
			try {
				userInput = in.nextInt();
				if (userInput <= 12 && userInput > 0) {
					return userInput;
				} else {
					System.out.println("\n*** "+userInput+" is not a valid option ***\n");
				}
			} catch (InputMismatchException e) {
				System.out.println("\n*** "+userInput+" is not a valid option ***\n");
			}
		}
	}
	
	private boolean getUtilities() {
		while(true) {
			String userInput = null;
			boolean utilities = false;
			in = new Scanner(System.in);
			System.out.println("Do you require a utility hookup? (y/n)");
			
			try {
				userInput = in.nextLine();
				if (userInput.equalsIgnoreCase("y") || userInput.equalsIgnoreCase("yes")) {
					utilities = true;
					return utilities;
				} else if (userInput.equalsIgnoreCase("n") || userInput.equalsIgnoreCase("no")) {
					utilities = false;
					return utilities;
				} else {
					System.out.println("\n*** "+userInput+" is not a valid option ***\n");
				}
			} catch (Exception e) {
				System.out.println("\n*** "+userInput+" is not a valid option ***\n");
			}
		}
	}

	private int getRvLength() {
		while(true) {
			int userInput = 0;
			in = new Scanner(System.in);
			System.out.println("How long is your RV? (Enter 0 for no RV)");
			
			
			
			try {
				userInput = in.nextInt();
				
				if (userInput >= 0) {
					return userInput;
				} else {
					System.out.println("\n*** "+userInput+" is not a valid option ***\n");
				}
			} catch (InputMismatchException e) {
				System.out.println("\n*** "+userInput+" is not a valid option ***\n");
			}
		}
	}

	private boolean getAccessible() {
		while(true) {
			String userInput = null;
			boolean access = false;
			in = new Scanner(System.in);
			System.out.println("Do you require handicap access? (y/n)");
			
			try {
				userInput = in.nextLine();
				if (userInput.equalsIgnoreCase("y") || userInput.equalsIgnoreCase("yes")) {
					access = true;
					return access;
				} else if (userInput.equalsIgnoreCase("n") || userInput.equalsIgnoreCase("no")) {
					access = false;
					return access;
				} else {
					System.out.println("\n*** "+userInput+" is not a valid option ***\n");
				}
			} catch (Exception e) {
				System.out.println("\n*** "+userInput+" is not a valid option ***\n");
			}
		}
	}
	
	private void printCampgrounds(List<Campground> campgrounds) {
		Formatter fmtCampHeader = new Formatter();
		int maxLength = 16;
		for(int i = 0; i < campgrounds.size(); i++) {
			Campground campground = campgrounds.get(i);
			if (campground.getName().length() > maxLength) {
				maxLength = campground.getName().length();
			}
		}
		
		fmtCampHeader.format("%-5s %-" + maxLength +"s %-12s %-12s %-6s", "", "Name", "Open Month", "Close Month", "Daily Fee");
		System.out.println();
		System.out.println(fmtCampHeader);
		
		for(int i = 0; i < campgrounds.size(); i++) {
			Campground campground = campgrounds.get(i);
			Formatter fmtCamps = new Formatter();
			fmtCamps.format("%-5s %-" + maxLength +"s %-12s %-12s %-6s", "ID# " + campground.getCampground_id(), campground.getName(), campground.getOpen_from_mm(), campground.getOpen_to_mm(), currencyFormat(campground.getDaily_fee()));
			System.out.println(fmtCamps);
		}
	}
	
	private void printReservations(List<Reservation> reservations) {
		Formatter fmtReservationsHeader = new Formatter();
		int maxLengthFamily = 16;
		int maxLengthCamp = 16;
		for(int i = 0; i < reservations.size(); i++) {
			Reservation reservation = reservations.get(i);
			if (reservation.getNameFamily().length() > maxLengthFamily) {
				maxLengthFamily = reservation.getNameFamily().length();
			}
			if (reservation.getNameCamp().length() > maxLengthCamp) {
				maxLengthCamp = reservation.getNameCamp().length();
			}
		}
		
		fmtReservationsHeader.format("%-10s %-"+ maxLengthFamily +"s %-12s %-"+ maxLengthCamp +"s %-12s %-12s", "ID", "Group Name", "Park", "Campground", "Start Date", "End Date");
		System.out.println();
		System.out.println(fmtReservationsHeader);
		
		for(int i = 0; i < reservations.size(); i++) {
			Reservation res = reservations.get(i);
			Formatter fmtReservations = new Formatter();
			fmtReservations.format("%-10s %-"+ maxLengthFamily +"s %-12s %-"+ maxLengthCamp +"s %-12s %-12s", res.getReservation_id(), res.getNameFamily(), res.getNamePark(), res.getNameCamp(), res.getFrom_date(), res.getTo_date());
			System.out.println(fmtReservations);
		}
	}
	
	private void printHeading(String headingText) {
		System.out.println("\n"+headingText);
		for(int i = 0; i < headingText.length(); i++) {
			System.out.print("-");
		}
		System.out.println();
	}
	
	private String currencyFormat(double n) {
		return NumberFormat.getCurrencyInstance().format(n);
	}
}
