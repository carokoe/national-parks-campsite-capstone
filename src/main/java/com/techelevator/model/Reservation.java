package com.techelevator.model;

import java.util.Date;

public class Reservation {
	
	private int reservation_id;
	private int site_id;
	private String nameFamily;
	private String namePark;
	private String nameCamp;
	private Date from_date;
	private Date to_date;
	private Date create_date;
	
	public int getReservation_id() {
		return reservation_id;
	}
	public void setReservation_id(int reservation_id) {
		this.reservation_id = reservation_id;
	}
	public int getSite_id() {
		return site_id;
	}
	public void setSite_id(int site_id) {
		this.site_id = site_id;
	}
	public String getNameFamily() {
		return nameFamily;
	}
	public void setNameFamily(String nameFamily) {
		this.nameFamily = nameFamily;
	}
	public String getNamePark() {
		return namePark;
	}
	public void setNamePark(String namePark) {
		this.namePark = namePark;
	}
	public String getNameCamp() {
		return nameCamp;
	}
	public void setNameCamp(String nameCamp) {
		this.nameCamp = nameCamp;
	}
	public Date getFrom_date() {
		return from_date;
	}
	public void setFrom_date(Date from_date) {
		this.from_date = from_date;
	}
	public Date getTo_date() {
		return to_date;
	}
	public void setTo_date(Date to_date) {
		this.to_date = to_date;
	}
	public Date getCreate_date() {
		return create_date;
	}
	public void setCreate_date(Date create_date) {
		this.create_date = create_date;
	}
	
}