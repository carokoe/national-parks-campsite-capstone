package com.techelevator.model.jdbc;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.model.Campground;
import com.techelevator.model.CampgroundDAO;

public class JDBCCampgroundDAO implements CampgroundDAO {
	
	private JdbcTemplate jdbcTemplate;
	
	public JDBCCampgroundDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Campground> getAllCampgrounds(int park_id) {
		ArrayList<Campground> campgrounds = new ArrayList<Campground>();
		
		String sqlFindCampground = "SELECT * FROM campground " +
									"WHERE park_id = ? "+
									"ORDER BY campground_id";
		
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlFindCampground, park_id);
		while(results.next()) {
			Campground campground = mapRowToCampground(results);
			campgrounds.add(campground);
		}
		return campgrounds;
	}
	
	@Override
	public Campground getCampFromId(int campground_id) {
		Campground campground = null;
		
		String sqlFindCamp = "SELECT * FROM campground " +
							 "WHERE campground_id = ?";
		
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlFindCamp, campground_id);
		if(results.next()) {
			campground = mapRowToCampground(results);
		} else {
			return null;
		}

		return campground;
	}
	
	private Campground mapRowToCampground(SqlRowSet results) {
		Campground campground = new Campground();
		campground.setCampground_id(results.getInt("campground_id"));
		campground.setDaily_fee(results.getDouble("daily_fee"));
		campground.setName(results.getString("name"));
		campground.setOpen_from_mm(results.getString("open_from_mm"));
		campground.setOpen_to_mm(results.getString("open_to_mm"));
		campground.setPark_id(results.getInt("park_id"));
		
		return campground;
	}

}
