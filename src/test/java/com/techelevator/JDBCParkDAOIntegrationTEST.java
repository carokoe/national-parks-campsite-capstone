package com.techelevator;

import org.junit.Assert;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.jdbc.datasource.SingleConnectionDataSource;

import com.techelevator.model.Park;
import com.techelevator.model.ParkDAO;
import com.techelevator.model.jdbc.JDBCParkDAO;

public class JDBCParkDAOIntegrationTEST {
	
	private static SingleConnectionDataSource dataSource;
	private ParkDAO dao;
	
	/* Before any tests are run, this method initializes the datasource for testing. */
	@BeforeClass
	public static void setupDataSource() {
		dataSource = new SingleConnectionDataSource();
		dataSource.setUrl("jdbc:postgresql://localhost:5432/campground");
		dataSource.setUsername("postgres");
		dataSource.setPassword("postgres1");
		/* The following line disables autocommit for connections 
		 * returned by this DataSource. This allows us to rollback
		 * any changes after each test */
		dataSource.setAutoCommit(false);
	}
	
	/* After all tests have finished running, this method will close the DataSource */
	@AfterClass
	public static void closeDataSource() throws SQLException {
		dataSource.destroy();
	}
	
	@Before
	public void setup() {
		dao = new JDBCParkDAO(dataSource);
	}

	/* After each test, we rollback any changes that were made to the database so that
	 * everything is clean for the next test */
	@After
	public void rollback() throws SQLException {
		dataSource.getConnection().rollback();
	}
	
	/* This method provides access to the DataSource for subclasses so that 
	 * they can instantiate a DAO for testing */
	protected DataSource getDataSource() {
		return dataSource;
	}
	
	@Test
	public void get_parks_test() {
		List<Park> parks = new ArrayList<Park>();
		
		parks = dao.getAllParks();
		
		Assert.assertEquals(3, parks.size());
	}
	
//	@Test
//	public void get_park_name() {
//		String[] parks;
//		
//		parks = dao.getParkNames();
//		
//		Assert.assertEquals("Acadia", parks[0]);
//	}

}
