package com.techelevator.view;

import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Scanner;

import com.techelevator.model.Campground;

public class Menu {

	private PrintWriter out;
	private Scanner in;

	public Menu(InputStream input, OutputStream output) {
		this.out = new PrintWriter(output);
		this.in = new Scanner(input);
	}

	public Object getChoiceFromOptions(Object[] options) {
		Object choice = null;
		while(choice == null) {
			displayMenuOptions(options);
			choice = getChoiceFromUserInput(options);
		}
		return choice;
	}
	
//	public Object getCampChoiceFromOptions(Campground[] options) {
//		Object choice = null;
//		while(choice == null) {
//			displayCampMenuOptions(options);
//			choice = getCampChoiceFromUserInput(options);
//		}
//		return choice;
//	}

	private Object getChoiceFromUserInput(Object[] options) {
		Object choice = null;
		String userInput = in.nextLine();
		if (userInput.equalsIgnoreCase("Q")) {
			System.exit(0);
		}
		try {
			int selectedOption = Integer.valueOf(userInput);
			if(selectedOption <= options.length && selectedOption > 0) {
				choice = options[selectedOption - 1];
			}
		} catch(NumberFormatException e) {
			out.println("\n*** "+userInput+" is not a valid option ***\n");		}
		if(choice == null) {
			out.println("\n*** "+userInput+" is not a valid option ***\n");
		}
		return choice;
	}
	
//	private Object getCampChoiceFromUserInput(Campground[] options) {
//		Object choice = null;
//		String userInput = in.nextLine();
//		try {
//			int selectedOption = Integer.valueOf(userInput);
//			if (selectedOption == 0) {
//				return null;
//			} else if(selectedOption <= options.length) {
//				choice = options[selectedOption - 1];
//			}
//		} catch(NumberFormatException e) {
//			// eat the exception, an error message will be displayed below since choice will be null
//		}
//		if(choice == null) {
//			out.println("\n*** "+userInput+" is not a valid option ***\n");
//		}
//		return choice;
//	}

	private void displayMenuOptions(Object[] options) {
		out.println();
		for(int i = 0; i < options.length; i++) {
			int optionNum = i+1;
			out.println(optionNum+") " + options[i]);
		}
		out.print("\nChoose an option (Q to quit) >>> ");
		out.flush();
	}
	
//	private void displayCampMenuOptions(Campground[] options) {
//		out.println();
//		for(int i = 0; i < options.length; i++) {
//			int optionNum = i+1;
//			out.println("#" + optionNum + " " + options[i].getName() + " " + options[i].getOpen_from_mm() + " " + options[i].getOpen_to_mm() + " $" + options[i].getDaily_fee());
//		}
//		out.print("\nPlease choose an option >>> ");
//		out.flush();
//	}
	
}
