package com.techelevator.model.jdbc;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.model.Park;
import com.techelevator.model.ParkDAO;

public class JDBCParkDAO implements ParkDAO {
	
	private JdbcTemplate jdbcTemplate;
	
	public JDBCParkDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Park> getAllParks() {
		ArrayList<Park> parks = new ArrayList<Park>();
		
		String sqlFindPark = "SELECT * FROM park " +
							 "ORDER BY name";
		
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlFindPark);
		while(results.next()) {
			Park park = mapRowToPark(results);
			parks.add(park);
		}
		return parks;
	}
	
	private Park mapRowToPark(SqlRowSet results) {
		Park park = new Park();
		park.setArea(results.getInt("area"));
		park.setDescription(results.getString("description"));
		park.setEstablish_date(results.getDate("establish_date"));
		park.setLocation(results.getString("location"));
		park.setName(results.getString("name"));
		park.setPark_id(results.getInt("park_id"));
		park.setVisitors(results.getInt("visitors"));
		
		return park;
	}

}
