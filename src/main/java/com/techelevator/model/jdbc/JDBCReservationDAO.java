package com.techelevator.model.jdbc;

import java.util.ArrayList;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.rowset.SqlRowSet;

import com.techelevator.model.Reservation;
import com.techelevator.model.ReservationDAO;

public class JDBCReservationDAO implements ReservationDAO {
	
	private JdbcTemplate jdbcTemplate;
	
	public JDBCReservationDAO(DataSource dataSource) {
		this.jdbcTemplate = new JdbcTemplate(dataSource);
	}

	@Override
	public List<Reservation> getUpcomingReservations(int park_id) {
		ArrayList<Reservation> reservations = new ArrayList<Reservation>();
		
		String sqlFindReservations = "SELECT reservation_id, reservation.site_id AS site_id, reservation.name AS nameFamily, p.name AS namePark, c.name AS nameCamp, from_date, to_date, create_date " +
									 "FROM reservation " +
									 "INNER JOIN site s ON reservation.site_id = s.site_id " +
									 "INNER JOIN campground c ON s.campground_id = c.campground_id " + 
									 "INNER JOIN park p ON c.park_id = p.park_id " +
									 "WHERE p.park_id = ? " +
									 "AND from_date BETWEEN CURRENT_DATE AND CURRENT_DATE + INTERVAL '30 day' " +
									 "AND to_date BETWEEN CURRENT_DATE AND CURRENT_DATE + INTERVAL '30 day' " +
									 "ORDER BY from_date";
	
		SqlRowSet results = jdbcTemplate.queryForRowSet(sqlFindReservations, park_id);
		while(results.next()) {
			Reservation reservation = mapRowToReservation(results);
			reservations.add(reservation);
		}
		return reservations;
	}
	
	private Reservation mapRowToReservation(SqlRowSet results) {
		Reservation reservation = new Reservation();
		reservation.setReservation_id(results.getInt("reservation_id"));
		reservation.setSite_id(results.getInt("site_id"));
		reservation.setNameFamily(results.getString("nameFamily"));
		reservation.setNamePark(results.getString("namePark"));
		reservation.setNameCamp(results.getString("nameCamp"));
		reservation.setFrom_date(results.getDate("from_date"));
		reservation.setTo_date(results.getDate("to_date"));
		reservation.setCreate_date(results.getDate("create_date"));	
		
		return reservation;
	}

}
